package com.mytest.spark.spark_test;

import org.apache.spark.Accumulator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.VoidFunction;

import java.util.List;
import java.util.regex.Pattern;

public final class AvgTemperatureAnalyzer {
  private static final Pattern SPACE = Pattern.compile(" ");

  public static void main(String[] args) throws Exception {
    SparkConf sparkConf = new SparkConf();
    sparkConf.set("spark.cores.max", "1");
    sparkConf.set("spark.executor.memory", "2048M");
    sparkConf.set("spark.driver.memory", "1024M");
    sparkConf.setAppName("JavaWordCount");
    sparkConf.setMaster("spark://tomask79.local:7077");
    JavaSparkContext ctx = new JavaSparkContext(sparkConf);
    ctx.addJar("/Users/tomask79/Documents/workspace/apache-spark-test2/spark-test/target/spark-test-0.0.1-SNAPSHOT.jar");
    JavaRDD<String> lines = ctx.textFile("hdfs://localhost:9000/user/weather.txt", 1);
    
    final Accumulator<Integer> tempSum = ctx.accumulator(0);
       
    lines.foreach(new VoidFunction<String>() {
		private static final long serialVersionUID = 1L;

		public void call(String t) throws Exception {
			tempSum.add(Integer.valueOf(SPACE.split(t)[1]));
		}
    });
    
    final Integer sum = tempSum.value();
    final long count = lines.count();
    final double avg = (double) sum / count;
    System.out.println("Average temperature "+avg);
      
    JavaRDD<String> result = lines.filter(new Function<String, Boolean>() {
		private static final long serialVersionUID = 1L;

		public Boolean call(String v1) throws Exception {
			final String arr[] = SPACE.split(v1);
			long temperature = Long.parseLong(arr[1]);
			return temperature <= avg;
		}
	});
    
    List<String> resultList = result.collect();
    for (String item: resultList) {
    	System.out.println("Result item: "+item);
    }
      
    ctx.stop();
  }
}
