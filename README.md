# Programming with Apache Spark, part 2 #

Last time I showed how Apache Spark RDD API can be used to solve simple problem with computing average temperature from file containing the weather forecast with eventual printing of rows with temperature lower then computed AVG. See:

```
https://bitbucket.org/tomask79/spark-programming/overview
```

Code was functional, but too chatty. Especially computing of average temperature from the whole set. Let's see how we can make the code to look cleaner with Apache Spark **Shared Variables**.

## Shared Variables in Apache Spark ##

Normally when you program Spark actions like map, any passed variables in there from driver program are then turned into local copies at remote workers. And any changes into them at remote workers **ARE NOT propagated back to the driver program**. To overcome this fact, Apache Spark offers concept of Shared Variables. We've got two types:

### Broadcast Variables ###

Broadcast variables are useful when you need **same data across of multiple Spark actions**. With calling **SparkContext.broadcast(v)**, "v" data are then **shipped to remote workers ONLY ONCE** in the serialized form, which speeds up the whole process.

### Acumulators ###

Are the variables where we can only "ADD" something to them, so they are perfectly usable for parallel processing. Important to remember is that **remote workers cannot read its value**, only add to them. Eventually ONLY driver program can then operate with its value.

## Demo of using Shared Variables ##

Accumulator is a perfect thing to solve our problem of sum of all temperatures from all rows. There is NO NEED to use map-reduce concept for it, like I did in my previous Spark programming part, we will simply use ** RDD forEach** function where we send parsed temperature to our accumulator. 


```
    final Accumulator<Integer> tempSum = ctx.accumulator(0);
       
    lines.foreach(new VoidFunction<String>() {
		private static final long serialVersionUID = 1L;

		public void call(String t) throws Exception {
			tempSum.add(Integer.valueOf(SPACE.split(t)[1]));
		}
    });
```

Counting of the average temperature looks then as following:

```
    final Integer sum = tempSum.value();
    final long count = lines.count();
    final double avg = (double) sum / count;
```

If you compile and run this repo, in the usual way, you should see the following result, like in previous case:


```
Average temperature 27.0
.
.
Result item: Berlin 20
Result item: Paris 15
Result item: Rome 25

```
For previous solution of the SAME PROBLEM using Spark RDD API based on map-reduce concept, again, take a look at:

```
https://bitbucket.org/tomask79/spark-programming/overview
```

Best Regards

Tomas